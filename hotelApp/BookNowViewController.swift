
//
//  BookNowViewController.swift
//  hotelApp
//
//  Created by Felix IT on 13/01/20.
//  Copyright © 2020 Felix IT. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class BookNowViewController: UIViewController {
    var tapGesture: UIGestureRecognizer!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var peopleTextField: UITextField!
    @IBOutlet weak var numberTextField: UITextField!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var scrollview: UIScrollView!
    public var selectedTable: Table?
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    @objc func keyboardWillShow(notification: Notification){
        scrollview.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 216, right: 0)
    }
    @objc func keyboardWillHide(notification: Notification){
        scrollview.contentInset = UIEdgeInsets.zero
        
    }
    @IBAction func OnTapBooking(_ sender: RoundButtonn) {
//       let ref = Database.database().reference().child("table")
//        let id = ref.childByAutoId().key
//        ref.child(id!).updateChildValues(["name" : self.nameTextField.text!, "numOfPeople" : self.peopleTextField.text!, "mobNum" : self.numberTextField.text!, "date" : self.dateTextField.text! ])
//            navigationController?.popViewController(animated: true)
        let ref = Database.database().reference().child("tables").child((selectedTable?.tableId)!)
        ref.updateChildValues(["isBooked": true]) { (er, ref) in
            if (er == nil) {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    @IBAction func OnTapCancel(_ sender: RoundButtonn) {
    }
}
