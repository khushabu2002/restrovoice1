
//
//  RoundButtonn.swift
//  hotelApp
//
//  Created by Felix IT on 08/01/20.
//  Copyright © 2020 Felix IT. All rights reserved.
//

import UIKit
@IBDesignable
class RoundButtonn: UIButton {

    @IBInspectable var cornerRadius: CGFloat = 0{
        didSet{
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0{
        didSet{
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear{
        didSet{
            self.layer.borderColor = borderColor.cgColor
        }
    }
}
