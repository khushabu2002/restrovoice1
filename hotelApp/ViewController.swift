//
//  ViewController.swift
//  hotelApp
//
//  Created by Felix IT on 07/01/20.
//  Copyright © 2020 Felix IT. All rights reserved.
//

import UIKit
import FirebaseDatabase

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        for i in 1...40 {
//            let ref = Database.database().reference().child("tables")
//            let id = ref.childByAutoId().key
//            ref/Users/felix-it06/Desktop/khushbu/hotelApp/hotelApp/ViewController.swift.child(id!).setValue(["tableId": id!, "tableName": "T" + String(i), "tableNumber": i, "isBooked": false])
//        }
    }
    @IBAction func OnTapTableBooking(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "TabBarController")
        navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func OnTapOnlineOrder(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "TabBarController1")
        navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func OnTapReadAboutUs(_ sender: UIButton) {
    }
}


struct Table {
    let isBooked: Bool!
    let tableId: String!
    let tableName: String!
    let tableNumber: Int!
}
