
//
//  MenuViewController.swift
//  hotelApp
//
//  Created by Felix IT on 13/01/20.
//  Copyright © 2020 Felix IT. All rights reserved.
//

import UIKit
import Cosmos

class MenuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var MenuTableView: UITableView!
 
    @IBOutlet weak var ratingstar: CosmosView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell") as! MenuTableViewCell
         cell.menuImages.image = #imageLiteral(resourceName: "burger")
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 253.0
    }
}
