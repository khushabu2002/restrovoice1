//
//  MenuTableViewCell.swift
//  hotelApp
//
//  Created by Felix IT on 13/01/20.
//  Copyright © 2020 Felix IT. All rights reserved.
//

import UIKit


class MenuTableViewCell: UITableViewCell {

    
  
    @IBOutlet weak var addtolist: UIButton!
    @IBOutlet weak var menuImages: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
