
//
//  TableStatusViewController.swift
//  hotelApp
//
//  Created by Felix IT on 08/01/20.
//  Copyright © 2020 Felix IT. All rights reserved.
//

import UIKit
import FirebaseDatabase
class TableStatusViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    var items: [Table] = []
    var isBooked = false
    @IBOutlet weak var tablesCollection: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.fetchTables()
    }
    func fetchTables() {
        let ref = Database.database().reference().child("tables")
        items = []
        ref.observeSingleEvent(of: .value) { (snapshot) in
            if(snapshot.exists()) {
                var tables: [Table] = []
                snapshot.children.forEach({ (child) in
                    let childValue = (child as! DataSnapshot).value as! [String: Any]
                    
                    let isBooked = childValue["isBooked"] as! Bool
                    let tableId = childValue["tableId"] as! String
                    let tableName = childValue["tableName"] as! String
                    let tableNumber = childValue["tableNumber"] as! Int
                    
                    let table = Table(isBooked: isBooked, tableId: tableId, tableName: tableName, tableNumber: tableNumber)
                    tables.append(table)
                })
                DispatchQueue.main.async {
                    self.items = tables
                    self.tablesCollection.reloadData()
                }
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TableCell", for: indexPath as IndexPath) as! TableStatusCollectionViewCell
        let table = self.items[indexPath.item]
       cell.TableLabel.text = table.tableName
        if table.isBooked {
            cell.backgroundColor = UIColor.red
        } else {
            cell.backgroundColor = UIColor.green
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "BookNowViewController") as! BookNowViewController
        vc.selectedTable = self.items[indexPath.item]
        navigationController?.pushViewController(vc, animated: true)
    }
    
    

}
